const { defineConfig } = require("cypress");
const getCompareSnapshotsPlugin = require('cypress-visual-regression/dist/plugin');

module.exports = defineConfig({
  env: {
    type: "actual",
    failSilently: false,
    SNAPSHOT_BASE_DIRECTORY: "snapshots/layoutAprovado",
    SNAPSHOT_DIFF_DIRECTORY: "snapshots/layoutAtual",
    ALWAYS_GENERATE_DIFF: false,
    cypressVisualRegressionConfig: {
      baseDirectory: "snapshots/layoutAprovado",
      diffDirectory: "snapshots/layoutAtual"
    }
  },
  screenshotsFolder: './cypress/screenshots',
  trashAssetsBeforeRuns: true,
  video: false,
  e2e: {
    setupNodeEvents(on, config) {
      getCompareSnapshotsPlugin(on, config);
    },
  },
});

