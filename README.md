# Projeto de código base para testes de regressão visual

Este é um projeto de testes automatizados de regressão visual.

Neste projeto, existe a possibilidade de executar os testes de regressão visual criando executando um comando para criar imagens base e depois executar outro comando comparando imagens recentemente criadas.

## Pré-requisitos

 Antes de começar, garanta que os seguintes sistemas estejam instalados em seu computador.

- [git](https://git-scm.com/) (estou usando a versão `2.35.2` enquanto escrevo este readme)
- [Node.js](https://nodejs.org/en/) (estou usando a versão `v18.16.0` enquanto escrevo este readme)
- npm (estou usando a versão `9.5.1` enquanto escrevo este readme)
- [Visual Studio Code](https://code.visualstudio.com/) (estou usando a versão `1.83.1` enquanto escrevo este readme) ou alguma outra IDE de sua preferência.

> **Obs. 1:** Ao instalar o Node.js o npm é instalado automaticamente.

> **Obs. 2:** Deixei links para os instaladores na lista de requisitos acima, caso não os tenha instalados ainda.

> **Obs. 3:** Para que os testes sejam executados, você precisará instalar.


## Inicialização e primeira execução do Cypress

1. - [ ] Na raiz do projeto, execute o comando `npm install`.
2. - [ ] Logo após, execute o comando `npx cypress open` para abrir o Cypress pela primeira vez.
3. - [ ] Ao executar o cypress pela primeira vez, forneça as permissões solicitadas.
4. - [ ] Clique em **E2E Testing** para executar no modo interativo. 
5. - [ ] Feche a janela e siga para as instruções de execução de scripts.

Agora que o _setup_ está pronto, clique [AQUI](./SCRIPTS.md) para visulizar as instruções para a execução dos testes de regressão visual.