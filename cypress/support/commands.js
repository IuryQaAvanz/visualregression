const compareSnapshotCommand = require('cypress-visual-regression/dist/command');

compareSnapshotCommand({
  capture: 'fullPage',
  fileName: (testName) => {
    return testName.split('describe(')[1].split(')')[0];
  }
});