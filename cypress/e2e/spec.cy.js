describe(`Suite de testes para ${Cypress.env('action')} imagens`, () => {
  it(`Acessa a página inicial e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
    cy.visit('http://localhost:8080/');
    cy.compareSnapshot('PaginaInicial', 0.1);
  });

  it.only(`Acessa o googledocs e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
    cy.visit('https://docs.google.com/document/d/1SCXvULBGJxcYWFLJ7lRGdDbJGeTxjte0qAbQTkFKn1o/edit?usp=sharing');
    cy.compareSnapshot('Google Docs', 0.1);
  });
});
