# Explicando o código e plugin utilizado no projeto...

Abaixo terão algumas explicações sobre o código e sobre como podemos utilizar o plugin cypress-visual-regression.

## Instalando o Cypress Visual Regression

Caso você tenha dificuldades de exeutar os testes deste projeto, você poderá instalar o plugin novamente através do comando `npm install cypress-visual-regression`

## Variáveis de ambiente

> **_Obs 1: NESTE PROJETO NÃO FORAM UTILIZADAS TODAS AS VARIÁVEIS POSSÍVEIS, PORÉM AQUI ESTÁ COMO PODEMOS UTILIZA-LAS PARA FINS ESPECÍFICOS._**

> O plugin cypress-visual-regression permite trabalhar com algumas variáveis de ambiente organizar melhor as evidências dos nossos testes. São elas:

| Variável de Ambiente            | Descrição                                                                                           |
|---------------------------------|-----------------------------------------------------------------------------------------------------|
| SNAPSHOT_BASE_DIRECTORY         | Diretório onde serão criadas imagens base                                                           |
| SNAPSHOT_DIFF_DIRECTORY         | Diretório onde serão criadas imagens com diferenças                                                 |
| INTEGRATION_FOLDER              | Usado para calcular e salvar no caminho definido como parâmetro em uma situação específica de teste |
| ALWAYS_GENERATE_DIFF            | Booleano, tem valor padrão `true`                                                                   |
| ALLOW_VISUAL_REGRESSION_TO_FAIL | Booleano, tem valor padrão `false`                                                                  |

> **Vamos entender o propósito de cada variável para entender melhor em que situação você deve configurar cada uma.**

**SNAPSHOT_BASE_DIRECTORY (Diretório Base de Snapshots):**

> **_Propósito:_** Indica o diretório principal onde seus snapshots serão armazenados.

> **_Exemplo de Uso:_** Você pode usar isso para definir um diretório global para todos os snapshots da sua aplicação.

**SNAPSHOT_DIFF_DIRECTORY (Diretório de Diferenças de Snapshots):**

> **_Propósito:_** Indica o diretório específico onde as diferenças entre os snapshots serão armazenadas.

> **_Exemplo de Uso:_** Ao executar testes de regressão visual, o Cypress pode armazenar as diferenças entre os snapshots nesta pasta para análise posterior.

**INTEGRATION_FOLDER (Pasta de Integração):**

> **_Propósito:_** Utilizada para calcular diretórios corretos de snapshots com base na integração ou contexto específico do teste.

> **_Exemplo de Uso:_** Se você tem diferentes áreas ou blocos em sua aplicação, pode usar essa variável para organizar os snapshots de acordo com esses blocos. Cada bloco terá seu próprio subdiretório dentro do diretório principal de snapshots.

**ALWAYS_GENERATE_DIFF:**

> **_Propósito:_** Controla se as imagens de diferença (diff) devem ser geradas para testes bem-sucedidos.

> **_Quando Configurar:_** Configure como true se você deseja que imagens de diferença sejam geradas para todos os testes, independentemente de terem sucesso ou falha. Configure como false se quiser gerar imagens de diferença apenas quando um teste falhar.

**ALLOW_VISUAL_REGRESSION_TO_FAIL:**

> **_Propósito:_** Controla se os testes falharão ou não quando uma diferença visual é detectada.

> **_Quando Configurar_**: Configure como true se você quiser que os testes continuem executando mesmo se diferenças visuais forem detectadas, permitindo a geração de imagens de diferença sem a falha do teste. Configure como false se desejar que um teste falhe quando uma diferença visual for detectada.

> ### **Exemplo de configuração da variável INTEGRATION_FOLDER:**
Suponha que você tenha uma aplicação com duas áreas principais: **frontend** e **backend**.

```
{
  "env": {
    "SNAPSHOT_BASE_DIRECTORY": "cypress/snapshots",
    "SNAPSHOT_DIFF_DIRECTORY": "cypress/snapshots-diff",
    "INTEGRATION_FOLDER": "blocos"
  }
}

```
Estrutura do projeto:
```
cypress
│
├── snapshots
│   ├── blocos
│   │   ├── frontend
│   │   │   ├── login
│   │   │   │   └── login-page.png
│   │   │   └── dashboard
│   │   │       └── dashboard-page.png
│   │   └── backend
│   │       ├── api
│   │       │   └── api-response.png
│   │       └── admin
│   │           └── admin-page.png
│   └── snapshots-diff
│       ├── login-page.png
│       └── dashboard-page.png
├── e2e
│   ├── frontend
│   │   ├── login
│   │   │   ├── login.spec.js
│   │   │   └── snapshots
│   │   └── dashboard
│   │       ├── dashboard.spec.js
│   │       └── snapshots
│   └── backend
│       ├── api
│       │   ├── api.spec.js
│       │   └── snapshots
│       └── admin
│           ├── admin.spec.js
│           └── snapshots
├── support
└── ...
```

Neste exemplo, as três variáveis são usadas para organizar snapshots de acordo com a área (frontend ou backend) e o blocos específicos dentro dessa área. O **INTEGRATION_FOLDER** é utilizado para salvar nos diretórios corretos com base nessas áreas e blocos.

> ### Exemplo de Testes para o Frontend:

**1. Teste login.spec.js em frontend:**
```
describe('Bloco Login', () => {
  it('Deve exibir a página de login', () => {
    cy.visit('/login');
    cy.get('h1').should('contain', 'Login Page');

    // Tirar snapshot específico para o bloco de login
    cy.screenshot({
      capture: 'fullPage',
      blackout: ['.sensitive-element'],
      path: `cypress/e2e/login/snapshots/login-page.png`
    });
  });

  // Outros testes relacionados ao login...


});
```

**2. Teste dashboard.spec.js em frontend:**
```
describe('Frontend - Bloco Painel', () => {
  it('Deve exibir o painel do usuário', () => {
    cy.visit('/dashboard');
    cy.get('h1').should('contain', 'User Dashboard');

    cy.screenshot({
      capture: 'fullPage',
      blackout: ['.sensitive-element'],
      path: 'cypress/snapshots/blocos/frontend/dashboard/dashboard-page.png'
    });
  });

  // Outros testes relacionados ao dashboard...
});
```

> ### Exemplo de Testes para o Backend:

**1. Teste api.spec.js em backend:**
```
describe('Backend - API', () => {
  it('Deve Obter a resposta da API', () => {
    // Simulação de uma chamada de API e teste de resposta
    cy.request('/api/data').then(response => {
      expect(response.status).to.eq(200);
    });

    cy.screenshot({
      capture: 'fullPage',
      blackout: ['.sensitive-element'],
      path: 'cypress/snapshots/blocos/backend/api/api-response.png'
    });
  });

  // Outros testes relacionados à API...
});
```

**2. Teste admin.spec.js em backend:**
```
describe('Backend - Página Administração', () => {
  it('Deve exibir a página de administração', () => {
    cy.visit('/admin');
    cy.get('h1').should('contain', 'Administração');

    cy.screenshot({
      capture: 'fullPage',
      blackout: ['.sensitive-element'],
      path: 'cypress/snapshots/blocos/backend/admin/admin-page.png'
    });
  });

  // Outros testes relacionados ao admin...
});
```

_Nestes exemplos, cada bloco tem seu próprio diretório dentro da pasta snapshots, e as evidências específicas são armazenadas nos subdiretórios chamados snapshots. Isso ajuda a organizar as evidências de acordo com os diferentes contextos de integração e torna mais fácil gerenciar e manter os testes visualmente. Note que a estrutura exata depende das suas necessidades específicas e da convenção de nomenclatura que você prefere para seus testes e evidências._

## Configurações deste projeto
> No arquivo _cypress.config.js_ localizado na raíz do projeto, estão as configurações padrões do cypress, onde serão inseridas as informações e parâmetros adicionais que o cypress executará como padrão no momento dos testes. Com base nas informações sobre variáveis de ambiente passadas anteriormente temos a seguinte estrutura de código:

```
const { defineConfig } = require("cypress");
const getCompareSnapshotsPlugin = require('cypress-visual-regression/dist/plugin');

module.exports = defineConfig({
  env: {
    type: "actual",
    failSilently: false,
    SNAPSHOT_BASE_DIRECTORY: "snapshots/layoutAprovado",
    SNAPSHOT_DIFF_DIRECTORY: "snapshots/layoutAtual",
    ALWAYS_GENERATE_DIFF: false,
    cypressVisualRegressionConfig: {
      baseDirectory: "snapshots/layoutAprovado",
      diffDirectory: "snapshots/layoutAtual"
    }
  },
  screenshotsFolder: './cypress/screenshots',
  trashAssetsBeforeRuns: true,
  video: false,
  e2e: {
    setupNodeEvents(on, config) {
      getCompareSnapshotsPlugin(on, config);
    },
  },
});
```

Configurações de ambiente:
```
env: {
  type: "actual",
  failSilently: false,
  SNAPSHOT_BASE_DIRECTORY: "snapshots/layoutAprovado",
  SNAPSHOT_DIFF_DIRECTORY: "snapshots/layoutAtual",
  ALWAYS_GENERATE_DIFF: false,
  cypressVisualRegressionConfig: {
    baseDirectory: "snapshots/layoutAprovado",
    diffDirectory: "snapshots/layoutAtual"
  }
},
```
1. - [ ] Definimos em `type:` o tipo de ambiente como "actual"
2. - [ ] Definimos em `failSilently: false` que os testes não falharão sem notificações do cypress e ele emitirá erros no console indicando que o teste não foi bem-sucessedido.
3. - [ ] Definimos em `cypressVisualRegressionConfig` a configuração específica para o plugin de comparação visual, indicando diretórios base e de diferença.
4. - [ ] Definimos em `screenshotsFolder:` o caminho onde o cypress irá salvar suas screenshots padrão do framework.
5. - [ ] Definimos em `trashAssetsBeforeRuns: true` que os ativos (como imagens) devem ser excluidas para evitar acúmulo de evidências desnecessárias.
6. - [ ] Definimos em `video: false` que a gravação de vídeos não será realizada.
7. - [ ] Definimos abaixo que a função **setupNodeEvents** irá adicionar o plugin **cypress-visual-regression** ao Cypress para que ele possa ser utilizado durante os testes de integração:
```
e2e: {
  setupNodeEvents(on, config) {
    getCompareSnapshotsPlugin(on, config);
  },
},

```

## Comando personalizado

> No arquivo _commands.js_ localizado na pasta _support_, existe um comando personalizado adicionado para realizar a coleta e comparação das imagens  para validação de layout.

```
const compareSnapshotCommand = require('cypress-visual-regression/dist/command');

compareSnapshotCommand({
  capture: 'fullPage',
  fileName: (testName) => {
    return testName.split('describe(')[1].split(')')[0];
  }
});
```

> O comando `cy.compareSnapshot()` é um comando customizado desenvolvido pelo plugin do Cypress Visual Regression, que permite capturar e comparar imagens. O comando acima está recendo como parâmetro padrão capturar a imagem da página inteira atreavés do `capture: 'fullPage'`, quando dentro do código não for passado nenhum parâmetro dentro do teste.  Note que existe outro parâmerto que defini como padrão: 

```
  fileName: (testName) => {
    return testName.split('describe(')[1].split(')')[0];
  }
```

> Por padrão o plugin cypress-visual-regression cria imagens com o nome do caminho completo de sua spec. O que esse comando faz, é deixar as imagens criadas dentro das pastas _layoutAprovado_ e _layoutAtual_ apenas com o nome da spec evitando uma imagem criada com excesso de caracteres e dificultando encontra-la no meio de outras evidências.

## Scripts

> **Abaixo temos o objeto scripts adicionado no arquivo _package.json_ localidado na raíz do projeto, nele, podemos agrupar comandos dentro de um unico comando para facilitar a execução dos testes.**

```
  "scripts": {
    "geraimgsbase": "cypress run --env type=base,action=coletar,motivoteste=coleta",
    "validalayout": "cypress run --env type=actual,action=comparar,motivoteste=compara"
  }
```

> Definimos em `geraimgsbase` _(COMANDO UTILIZADO PARA GERAR AS IMAGENS BASE)_ os parâmetros para que ele execute em modo headless através de `cyprepss run`, estamos enviado configurações adicionais padrão através do `--env`, em `action` ele enviará a informação dentro do describe com a ação que está sendo executada, em `motivoteste` ele enviará o motivo do teste dentro do bloco de teste `it('') `.

**OBS.: O parâmetro `type=base` é obrigatório neste caso para gerar as imagens base por dfinição do plugin cypress-visual-regression.**

**Como utilizar:** No seu terminal, execute o seguinte comando: `npm run geraimgsbase`

> Definimos em `validalayout` _(COMANDO UTILIZADO PARA GERAR NOVAS IMAGENS E COMPARAR COM AS IMAGENS BASE FAZENDO A VALIDAÇÃO DO LAYOUT)_ os parâmetros para que ele execute em modo headless através de `cyprepss run`, estamos enviado configurações adicionais padrão através do `--env`, em `action` ele enviará a informação dentro do describe com a ação que está sendo executada, em `motivoteste` ele enviará o motivo do teste dentro do bloco de teste `it('') `.

**OBS.: O parâmetro `type=actual` é obrigatório neste caso para gerar novas imagens e comparar as imagens criadas com as imagens base por dfinição do plugin cypress-visual-regression.**

**Como utilizar:** No seu terminal, execute o seguinte comando: `npm run validalayout`

## O teste
> Abaixo podemos ver um dos testes criados para coletar imagens e também para comparar imagens. 

```
describe(`Suite de testes para ${Cypress.env('action')} imagens`, () => {
  it(`Acessa a página e ${Cypress.env('motivoteste')} imagens para validar layout`, () => {
    cy.visit('https://umaurlqualquer.com/');
    cy.compareSnapshot('Página', 0.1);
  });
});

```
> Dentro do describe está sendo inserido o seguinte trecho: `${Cypress.env('action')}` e  dentro do teste o trecho: `${Cypress.env('motivoteste')}`, esses trechos estão vindo de dentro do arquivo _package.json_. Na sessão scripts, tenho um comando que poderá ser acionado através do `npm run geraimgsbase`, esse comando está recebendo como parâmetro a seguinte sequencia: `"geraimgsbase": "cypress run --env type=base,action=coletar,motivoteste=coleta"`, esse comando gera imagens base para serem comapradas porteriormente, passando as intruções de _action_ e _motivoteste_ dentro do script, eu consigo utilizar o mesmo teste para executar a coleta de imagens e a comparação de imagens. 

> Para comparar as imagens o comando `npm run validalayout` recebe como parâmetro a seguinte sequencia: `"cypress run --env type=actual,action=comparar,motivoteste=compara"`

> O comando `cy.compareSnapshot('Página', 0.1)` está vindo do arquivo _commands.js_, ele está recebendo como como parâmetro o nome da imagem que será gerada e uma margem de aceite para que o teste passe. Neste caso estou definindo que tem uma margem de erro para aceite de 0,1%. Os valores podem ser alterados caso necessário. O valor pode variar de 0,00 (sem diferença) a 1,00 (cada pixel é diferente). Assim você pode inserir um limite de erro de 0,40, o teste falhará somente se 40% dos pixels forem diferentes. 

Exemplo:

| Limite | Falha quando |
|--------|--------------|
| .25 | > 25% |
| .30 | > 30% |
| .50 | > 50% |
| .75 | > 75% |


## Execução de testes

> Abaixo segue um exemplo gráfico do teste rodando e gerando uma imagem que servirá de base para comparação posterior. Neste exemplo, ele está acessando uma página que contem um documento de texto criado no Google Docs contendo um texto e uma imagem. O cypress acessa a página e tira um print utilizando o comando personalizado `cy.compareSnapshot()`.

![gerandoimagens](./imgbase1.gif)

> Abaixo segue um exemplo gráfico do teste rodando novamente, desta vez o teste passa, estamos executando aqui o comando `npm run validalayout`, note que neste caso evidências não foram geradas, pois assim como vemos na explicação do código e plugin, estou utilizando uma variável de ambiente **`ALWAYS_GENERATE_DIFF`** recebendo o parâmetro `false` para que ele crie imagens apenas se forem detectadas alterações nas imagens comparadas.

![comparandoimagens](./compara.gif)

> Agora vamos excluir a imagem com a logo do Cypress do arquivo doc criado no Google Docs e rodar novamente o teste executando mais uma vez o comando `npm run validalayout`. Note que o teste apresenta um erro avisando que foram detectadas alterações na imagem comparada, desta vez, ele gerou uma evidência na pasta `layoutAtual`, ao entrarmos na pasta, podemos ver destacado em vermelho a diferença encontrada, neste caso ele não identificou na imagem, a logo do Cypress e destacou em uma evidência criada na pasta `layoutAtual` para que possamos analisar os erros encontrados. 

![gerandoEvidencias](./diff.gif)
